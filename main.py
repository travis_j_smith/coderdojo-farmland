import random
import pygame

class Player(pygame.sprite.Sprite):
    def __init__(self):
        self.image = pygame.image.load('images/player.png').convert()
        self.image.set_colorkey((255, 255, 255))
        self.x = 0
        self.y = 0

    def move(self, direction, grid):
        can_move = True
        new_pos_x = self.x
        new_pos_y = self.y

        if direction == "up" and self.y > 0:
            new_pos_y -= 1
        elif direction == "down" and self.y < len(grid) - 1:
            new_pos_y += 1
        elif direction == "left" and self.x > 0:
            new_pos_x -= 1
        elif direction == "right" and self.x < len(grid[0]) - 1:
            new_pos_x += 1

        # if we are moving within the grid -- IE the position is within the arrays
        if can_move:
            new_loc = grid[new_pos_x][new_pos_y]

            # if the sprite on the grid item is passable, or there is no sprite, then move there
            if new_loc.sprite is None or not hasattr(new_loc.sprite, "passable") or new_loc.sprite.passable:
                grid[self.x][self.y].player = None
                self.x = new_pos_x
                self.y = new_pos_y
                grid[self.x][self.y].player = self
                grid[self.x][self.y].collide()


class Wheat(pygame.sprite.Sprite):
    def __init__(self):
        self.image = pygame.image.load('images/wheat.png').convert()
        self.image.set_colorkey((255, 255, 255))
        self.passable = False

    def action(self, parent):
        parent.sprite = Seed()


class Seed(pygame.sprite.Sprite):
    def __init__(self):
        self.image = pygame.image.load('images/seed.png').convert()
        self.image.set_colorkey((255, 255, 255))

    def collide(self, parent):
        parent.sprite = None

class GridItem():
    def __init__(self, x, y, width, height):
        self.rect = pygame.Rect(x, y, width, height)
        self.colour = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.width = 1
        self.sprite = None
        self.player = None

    def set_sprite(self, sprite):
        self.sprite = sprite

    def draw(self, screen):
        if self.sprite is not None:
            screen.blit(self.sprite.image, self.rect)

        if self.player is not None:
            screen.blit(self.player.image, self.rect)

    # action is done when clicking on a grid item
    def action(self):
        if self.sprite is not None and hasattr(self.sprite, "action"):
            self.sprite.action(self)

    # collide is when the player moves onto the grid item
    def collide(self):
        if self.sprite is not None and hasattr(self.sprite, "collide"):
            self.sprite.collide(self)

class Game:
    def run(self, rows, cols, grid_size):
        pygame.init()
        window_surface = pygame.display.set_mode((rows * grid_size, cols * grid_size), 0, 32)
        pygame.display.set_caption('Farmland')

        game_clock = pygame.time.Clock()

        # initialize the grid. Right now it just randomly puts wheat on 15 percent of spaces
        grid = []
        for i in range(0, rows):
            row = []
            for x in range(0, cols):
                to_add = GridItem(i * grid_size, x * grid_size, grid_size, grid_size)

                if random.randint(0, 100) < 15:
                    to_add.set_sprite(Wheat())

                row.append(to_add)

            grid.append(row)

        running = True

        player = Player()
        grid[0][0].player = player
        while running:
            window_surface.fill((0, 110, 0))
            x_click = -1
            y_click = -1

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    x_click, y_click = event.pos
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        player.move("left", grid)
                    elif event.key == pygame.K_RIGHT:
                        player.move("right", grid)
                    elif event.key == pygame.K_UP:
                        player.move("up", grid)
                    elif event.key == pygame.K_DOWN:
                        player.move("down", grid)

            for row in grid:
                for col in row:
                    col.draw(window_surface)

                    # check if we clicked the grid item
                    if col.rect.collidepoint(x_click, y_click):
                        col.action()


            pygame.display.update()
            game_clock.tick(120)

Game().run(20, 20, 40)